/* YourDuino SKETCH UltraSonic Serial 2.0
 Runs HC-04 and SRF-06 and other Ultrasonic Modules
 Open Serial Monitor to see results
 Reference: http://playground.arduino.cc/Code/NewPing
 Questions?  terry@yourduino.com */

#include <NewPing.h>   //UltraSonic
#include <Wire.h>     //LCD
#include <Adafruit_RGBLCDShield.h>    //LCD
#include <utility/Adafruit_MCP23017.h>    //LCD


// The shield uses the I2C SCL and SDA pins. On classic Arduinos
// this is Analog 4 and 5 so you can't use those for analogRead() anymore
// However, you can connect other I2C sensors to the I2C bus and share
// the I2C bus.
Adafruit_RGBLCDShield lcd = Adafruit_RGBLCDShield();


/* ************************************************
   Macros
 * ************************************************/

/* ************************************************
   Constants
 * ************************************************/
//UltraSonic
#define  TRIGGER_PIN  11
#define  ECHO_PIN     12
#define MAX_DISTANCE 200 // Maximum distance we want to ping for (in centimeters).
                         //Maximum sensor distance is rated at 400-500cm.

//LCD backlight color
#define RED 0x1
#define YELLOW 0x3
#define GREEN 0x2
#define TEAL 0x6
#define BLUE 0x4
#define VIOLET 0x5
#define WHITE 0x7


//Calibration
const uint32_t CALIBRATION_PERIOD_MS = 2000;    //Amount of time the device needs to calibrate itself to detect it's position
const int SENSOR_MOTION_DETECT_IN = 2;   //Distance / height to trigger a new measurment; detects if someone is under the sensor
const int SENSOR_ERROR_IN = 0;        //Sensor measurment error. TODO: Determin value
/* ************************************************
  Global variabls
* ************************************************/
//UltraSonic
NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE); // NewPing setup of pins and maximum distance.
int sensor_position_in;
bool calibrated = false;      //Flag that marks if the system has detected it's current position
static uint32_t calib_period_counter = 0;

/* ************************************************
  Function declaration
* ************************************************/
void lcd_button_handler();
void detect_position();

/* ************************************************
  Function definitions
* ************************************************/
/*
  Determine the current position of the sensor. This is used to initially determine
  the height of the sensor. After a continuous measurment has been read for the
  duration of the calibration period, the sensor position is set and the calibrated
  flag should be set.

  1. reads sensor position
  2. checks if sensor position has changed
  3. if the sensor position (or measurment) hasn't changed, then update the counter
  4. If the counter is greater-than or equal-to the calibration period, then set the
     calibrated flag

*/
void detect_position() {

  uint32_t current_time = millis();
  int current_sensor_pos = sonar.ping_in() + SENSOR_ERROR_IN;

  Serial.println(calib_period_counter);
  Serial.print("current: ");
  Serial.println(current_sensor_pos);
  Serial.print("calibrated: ");
  Serial.println(sensor_position_in);

  //Sensor reading is constant
  if (current_sensor_pos >= sensor_position_in - 1
      &&  current_sensor_pos <= sensor_position_in + 1) {

    Serial.print("elapsed: ");
    Serial.println((current_time - calib_period_counter));

    if ((calib_period_counter) >= CALIBRATION_PERIOD_MS
        && calib_period_counter != 0) {

      calibrated = true;

      lcd.clear();
      lcd.setCursor(0,0);
      lcd.print("System");
      lcd.setCursor(0,1);
      lcd.print("Calibrated");
      lcd.setCursor(0,0);
      delay(5000);
      lcd.clear();

    }

    calib_period_counter += current_time - calib_period_counter;

  //Sensor reading changed
  } else {
    sensor_position_in = current_sensor_pos;
    calib_period_counter = 0;
  }


}

/*
  Setup method.

  Runs once
*/
void setup() {
  Serial.begin(9600);
  Serial.println("UltraSonic Distance Measurement");

  //LCD setup
  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);

  // Print a message to the LCD. We track how long it takes since
  // this library has been optimized a bit and we're proud of it :)
  lcd.setBacklight(RED);


  //Static sensor settings (disable auto set)
  calibrated = true;
  sensor_position_in = 83;
}

/*
  LCD button handler
  Resets screen
  Changes the LCD backlight color with the arrow buttons
*/
void lcd_button_handler() {
  uint8_t buttons = lcd.readButtons();

  delay(100);
  if (buttons) {
    if (buttons & BUTTON_UP) {
      lcd.setBacklight(RED);
    }
    if (buttons & BUTTON_DOWN) {
      lcd.setBacklight(YELLOW);
    }
    if (buttons & BUTTON_LEFT) {
      lcd.setBacklight(GREEN);
    }
    if (buttons & BUTTON_RIGHT) {
      lcd.setBacklight(TEAL);
    }
    if (buttons & BUTTON_SELECT) {
      //lcd.setBacklight(VIOLET);
      lcd.clear();
      lcd.setCursor(0,0);
    }
  }
}

/*
  Main program control loop,

  LOOP: RUNS CONSTANTLY
*/
void loop() {

  if (!calibrated) {
    delay(100);
    detect_position();
    
  } else {
    //Measure distance
    delay(30);// Wait 100ms between pings (about 10 pings/sec). 29ms should be the shortest delay between pings.

    int distance_in = sonar.ping_in();
    if (distance_in <= (sensor_position_in - SENSOR_MOTION_DETECT_IN)) {
      int height_diff = sensor_position_in - distance_in;   //height difference in inches
      int diff_ft = height_diff / 12;
      int diff_in = height_diff % 12;

      //Dispay values on the LCD
      lcd.clear();
      lcd.setCursor(0,0);               //col: 0  line: 0
      lcd.print("Height:");

      lcd.setCursor(0, 1);              //col: 0  line: 1
      lcd.print(diff_ft);
      lcd.print(" ft ");
      lcd.print(diff_in);
      lcd.print(" in");

      lcd.setCursor(0,0);               //Return home
    }


  }


  //Handle backlight color change events
  lcd_button_handler();


}
