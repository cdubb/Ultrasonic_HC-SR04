Ultrasonic_HC-SR04
------------------
Ultrasonic sensor Arduino project.

This project uses the _NewPing_ library by default.


### Directory: misc
__Excluded from build__

Alternative sketches that don't use _NewPing_ are in the _misc/alternatives_ folder and excluded from the build. These sketches are for examples. If you want to use them, swap out the main source file _Ultrasonic_HC-SR04.cpp_ in the _src_ directory.



### Build Commands

Clean project
```
pio run -e uno -t clean
```

Dump current build environment
```
pio run  -e uno --target=envdump
```

Build (will upload if auto upload is enabled, see _platformio.ini_)
```
pio run -e uno
```

Build and upload
```
pio run -e uno --target=upload
```
