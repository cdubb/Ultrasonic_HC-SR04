/* YourDuino SKETCH UltraSonic Serial 2.0
 Runs HC-04 and SRF-06 and other Ultrasonic Modules
 Open Serial Monitor to see results
 Reference: http://playground.arduino.cc/Code/NewPing
 Questions?  terry@yourduino.com */


#include <Servo.h>
#include <NewPing.h>

/* ************************************************
   Macros
 * ************************************************/
 #define RANDSERVO_ANGLE(angle) \
  angle = random(0, 360); \
  if (angle > 180) { angle = -1* (angle - 180); }

/* ************************************************
   Constants
 * ************************************************/
//UltraSonic
#define  TRIGGER_PIN  11
#define  ECHO_PIN     12
#define MAX_DISTANCE 200 // Maximum distance we want to ping for (in centimeters).
                         //Maximum sensor distance is rated at 400-500cm.

//Servo
const int X_AXIS_SERVO_PIN = 5;  //x-axis servo on the gimbal. green wire
const int Y_AXIS_SERVO_PIN = 6;  //y-axis servo on the gimbal. yellow wire

//control
const int LOOP_COUNT_TRIGGER = 5;       //Number of main loop iterations before a move servo event is triggered

/* ************************************************
  Global variabls
* ************************************************/
//UltraSonic
NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE); // NewPing setup of pins and maximum distance.
int distance_in;
int distance_cm;

//Servo

/*
  Servo control object
*/
struct servo_ctrl_t {
  Servo servo;
  int angle;    // servo angle position
};

enum servo_axis_t {X_AXIS, Y_AXIS};     //References
servo_ctrl_t servos[2];
/*
Servo x_axis_servo;
Servo y_axis_servo;
enum X_AXIS, Y_AXIS;
int[] servo_pos = {0, 0};     // servo angle position
*/
//int x_axis_servo_angle = 0;   // x-axis servo position in degrees
//int y_axis_servo_angle = 0;   // y-axis servo position in degrees

/* ************************************************
  Function declaration
* ************************************************/
void move_servo(servo_axis_t axis, int angle, int delay_ms);

void move_gimbal_random(); //Move the gimbal to some random position

/* ************************************************
  Function definitions
* ************************************************/

/*
  Move a servo. Updates the angle on the servo_ctrl

  If the new angle is greater than 180 degrees, then don't move the servo.
*/
void move_servo(servo_axis_t axis, int new_angle, int delay_ms) {
  servo_ctrl_t servo_ctrl = servos[axis];

  int servoAngle = servo_ctrl.angle;
  servo_ctrl.servo.write(45);      // Turn SG90 servo Left to 50 degrees
delay(1000);          // Wait 1 second
servo_ctrl.servo.write(90);      // Turn SG90 servo back to 90 degrees (center position)
delay(1000);          // Wait 1 second
servo_ctrl.servo.write(135);     // Turn SG90 servo Right to 135 degrees
delay(1000);          // Wait 1 second
servo_ctrl.servo.write(90);      // Turn SG90 servo back to 90 degrees (center position)
delay(1000);

//end control the servo's direction and the position of the motor


//control the servo's speed

//if you change the delay value (from example change 50 to 10), the speed of the servo changes
for(servoAngle = 0; servoAngle < 180; servoAngle++)  //move the micro servo from 0 degrees to 180 degrees
{
 servo_ctrl.servo.write(servoAngle);
 delay(50);
}

for(servoAngle = 180; servoAngle > 0; servoAngle--)  //now move back the micro servo from 0 degrees to 180 degrees
{
 servo_ctrl.servo.write(servoAngle);
 delay(10);
}
/*
  //Handle bad input
  if (abs(new_angle) > 180) {
    return;
  }

  if (new_angle > servos[axis].angle) {
    for(;servos[axis].angle <= new_angle; servo_ctrl.angle++) {
        Serial.print("Angle: ");
        Serial.println(servo_ctrl.angle);
        servo_ctrl.servo.write(servo_ctrl.angle);
        delay(delay_ms);
    }
  } else if (new_angle < servo_ctrl.angle) {
    for(;servo_ctrl.angle >= new_angle; servo_ctrl.angle--) {
        Serial.print("Angle: ");
        Serial.println(servo_ctrl.angle);
        servo_ctrl.servo.write(servo_ctrl.angle);
        delay(delay_ms);
    }
  }*/
}


/*
  Move the gimbal to some random position
*/
void move_gimbal_random() {
  //Move X
  int x_new_angle = random(0, 45);
  move_servo(X_AXIS, x_new_angle, 100);

  //Move Y
  int y_new_angle = random(0, 45);
  move_servo(Y_AXIS, y_new_angle, 100);
}


/*
  Setup method.

  Runs once
*/
void setup() {
  Serial.begin(9600);
  Serial.println("UltraSonic Distance Measurement");

  //for gimbal random movement function
  randomSeed(analogRead(0));

  //Setup servo
  servos[X_AXIS].servo.attach(X_AXIS_SERVO_PIN);
  servos[X_AXIS].angle = 0;
  servos[X_AXIS].servo.write(servos[X_AXIS].angle);

  servos[Y_AXIS].servo.attach(Y_AXIS_SERVO_PIN);
  servos[Y_AXIS].angle = 0;
  servos[Y_AXIS].servo.write(servos[Y_AXIS].angle);
  /*
  x_axis_servo.attach(X_AXIS_SERVO_PIN);
  x_axis_servo.write(servo_pos[x_])
  y_axis_servo.attach(Y_AXIS_SERVO_PIN);
  */
}

/*
  Main program control loop,

  LOOP: RUNS CONSTANTLY
*/
void loop() {
  int count = 0;

  delay(100);// Wait 100ms between pings (about 10 pings/sec). 29ms should be the shortest delay between pings.
  distance_in = sonar.ping_in();
  Serial.print("Ping: ");
  Serial.print(distance_in); // Convert ping time to distance and print result
                            // (0 = outside set distance range, no ping echo)
  Serial.print(" in     ");

  delay(100);// Wait 100ms between pings (about 10 pings/sec). 29ms should be the shortest delay between pings.
  distance_cm = sonar.ping_cm();
  Serial.print("Ping: ");
  Serial.print(distance_cm);
  Serial.println(" cm");

  //Check for trigger and call function to move the gimbal
  if (count == LOOP_COUNT_TRIGGER) {
    move_gimbal_random();
    count = 0;
  } else {
    count++;
  }

}
