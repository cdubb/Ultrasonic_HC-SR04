/* YourDuino SKETCH UltraSonic Serial 2.0
 Runs HC-04 and SRF-06 and other Ultrasonic Modules
 Open Serial Monitor to see results
 Reference: http://playground.arduino.cc/Code/NewPing
 Questions?  terry@yourduino.com */


#include <NewPing.h>   //UltraSonic

/* ************************************************
   Macros
 * ************************************************/

/* ************************************************
   Constants
 * ************************************************/
//UltraSonic
#define  TRIGGER_PIN  11
#define  ECHO_PIN     12
#define MAX_DISTANCE 200 // Maximum distance we want to ping for (in centimeters).
                         //Maximum sensor distance is rated at 400-500cm.

/* ************************************************
  Global variabls
* ************************************************/
//UltraSonic
NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE); // NewPing setup of pins and maximum distance.
int distance_in;
int distance_cm;


/* ************************************************
  Function declaration
* ************************************************/

/* ************************************************
  Function definitions
* ************************************************/

/*
  Setup method.

  Runs once
*/
void setup() {
  Serial.begin(9600);
  Serial.println("UltraSonic Distance Measurement");
}

/*
  Main program control loop,

  LOOP: RUNS CONSTANTLY
*/
void loop() {

  //Measure distance
  delay(100);// Wait 100ms between pings (about 10 pings/sec). 29ms should be the shortest delay between pings.
  distance_in = sonar.ping_in();
  Serial.print("Ping: ");
  Serial.print(distance_in); // Convert ping time to distance and print result
                            // (0 = outside set distance range, no ping echo)
  Serial.print(" in     ");

  delay(100);// Wait 100ms between pings (about 10 pings/sec). 29ms should be the shortest delay between pings.
  distance_cm = sonar.ping_cm();
  Serial.print("Ping: ");
  Serial.print(distance_cm);
  Serial.println(" cm");

}
