/* YourDuino SKETCH UltraSonic Serial 2.0
 Runs HC-04 and SRF-06 and other Ultrasonic Modules
 Open Serial Monitor to see results
 Reference: http://playground.arduino.cc/Code/NewPing
 Questions?  terry@yourduino.com */


#include <NewPing.h>   //UltraSonic
#include <Wire.h>     //LCD
#include <Adafruit_RGBLCDShield.h>    //LCD
#include <utility/Adafruit_MCP23017.h>    //LCD


// The shield uses the I2C SCL and SDA pins. On classic Arduinos
// this is Analog 4 and 5 so you can't use those for analogRead() anymore
// However, you can connect other I2C sensors to the I2C bus and share
// the I2C bus.
Adafruit_RGBLCDShield lcd = Adafruit_RGBLCDShield();


/* ************************************************
   Macros
 * ************************************************/

/* ************************************************
   Constants
 * ************************************************/
//UltraSonic
#define  TRIGGER_PIN  11
#define  ECHO_PIN     12
#define MAX_DISTANCE 200 // Maximum distance we want to ping for (in centimeters).
                         //Maximum sensor distance is rated at 400-500cm.

 //LCD backlight color
 #define RED 0x1
 #define YELLOW 0x3
 #define GREEN 0x2
 #define TEAL 0x6
 #define BLUE 0x4
 #define VIOLET 0x5
 #define WHITE 0x7
/* ************************************************
  Global variabls
* ************************************************/
//UltraSonic
NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE); // NewPing setup of pins and maximum distance.
int distance_in;
int distance_cm;


/* ************************************************
  Function declaration
* ************************************************/
void lcd_change_backlight();


/* ************************************************
  Function definitions
* ************************************************/

/*
  Setup method.

  Runs once
*/
void setup() {
  Serial.begin(9600);
  Serial.println("UltraSonic Distance Measurement");

  //LCD setup
  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);

  // Print a message to the LCD. We track how long it takes since
  // this library has been optimized a bit and we're proud of it :)
  lcd.setBacklight(WHITE);
}

/*
  Changes the LCD backlight color with the arrow buttons
*/
void lcd_change_backlight() {
  uint8_t buttons = lcd.readButtons();

  if (buttons) {
    if (buttons & BUTTON_UP) {
      lcd.setBacklight(RED);
    }
    if (buttons & BUTTON_DOWN) {
      lcd.setBacklight(YELLOW);
    }
    if (buttons & BUTTON_LEFT) {
      lcd.setBacklight(GREEN);
    }
    if (buttons & BUTTON_RIGHT) {
      lcd.setBacklight(TEAL);
    }
    if (buttons & BUTTON_SELECT) {
      lcd.setBacklight(VIOLET);
    }
  }
}

/*
  Main program control loop,

  LOOP: RUNS CONSTANTLY
*/
void loop() {

  //Measure distance
  delay(100);// Wait 100ms between pings (about 10 pings/sec). 29ms should be the shortest delay between pings.
  distance_in = sonar.ping_in();
  //Serial.print("Ping: ");
  //Serial.print(distance_in); // Convert ping time to distance and print result
                            // (0 = outside set distance range, no ping echo)
  //Serial.print(" in     ");

  delay(100);// Wait 100ms between pings (about 10 pings/sec). 29ms should be the shortest delay between pings.
  distance_cm = sonar.ping_cm();
  //Serial.print("Ping: ");
  //Serial.print(distance_cm);
  //Serial.println(" cm");

  //Dispay values on the LCD
  lcd.clear();
  lcd.setCursor(0,0);               //col: 0  line: 0
  lcd.print(distance_in);
  lcd.print(" in");

  lcd.setCursor(0, 1);              //col: 0  line: 1
  lcd.print(distance_cm);
  lcd.print(" cm");

  lcd.home();


  //Handle backlight color change events
  lcd_change_backlight();
}
